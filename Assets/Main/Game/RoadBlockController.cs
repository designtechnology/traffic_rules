﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoadBlockController : MonoBehaviour {

	private StageController _stageController;

	private int moveCount = 0;

	public int index = 100;

	void Awake(){
		_stageController = this.transform.parent.parent.GetComponent<StageController> ();
	}

	void Update(){
		if(moveCount < 3)
			moveCount++;
		CheckDisappeared ();
	}

	private void CheckDisappeared(){
		if (index == 0 && this.transform.position.y + 400f < 400f * _stageController.adjustedRate) {
			_stageController.StopStage ();
		}
		else if (this.transform.position.y + 800f < -400f * _stageController.adjustedRate) {
			_stageController.AddStage ();
			_stageController.RemoveStage (this.gameObject);
		}
	}

	void OnTriggerEnter2D (Collider2D col){
		if (moveCount < 2) {
			Destroy (col.gameObject);
		}
	}
}
