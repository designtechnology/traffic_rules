﻿using UnityEngine;
using System.Collections;

public class EvilHuman : EvilBehaviour {

	public enum EvilType
	{
		RANDOM,
		SMARTPHONE,
		TOBACCO,
		POISUTE
	}

	public EvilType _state = EvilType.RANDOM;

	private bool _poiFlg = false; //ポイ捨てしたかどうか

	protected override void Awake () {
		base.Awake ();
		if(_state == EvilType.RANDOM)
			_state = (EvilType)Random.Range (1, System.Enum.GetNames(typeof(EvilType)).Length);
	}

	void Start() {
		AddEvilAbillity ();
	}

	void Update() {
		if (_state == EvilType.POISUTE && !_poiFlg) {
			//道の真ん中あたりでポイ捨てする
			if (this.transform.position.y < 50) {
				DoPoisute ();
				_poiFlg = true;
			}
		}
	}

	protected override void AddEvilAbillity (){
		switch (_state) {
		case EvilType.SMARTPHONE:
			chara.point = 30;
			_animateSprite = _spriteLoader.GetSpriteArray (SpriteLoader.CharacterType.SMARTPHONE);
			_characterAnimator.PlayAnimation (_animateSprite, 0);
			break;
		case EvilType.TOBACCO:
			chara.point = 70;
			_animateSprite = _spriteLoader.GetSpriteArray (SpriteLoader.CharacterType.TOBACCO);
			_characterAnimator.PlayAnimation (_animateSprite, 0);
			break;
		case EvilType.POISUTE:
			chara.point = 40;
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (200, 300);
			WalkPoisute ();
			break;
		default:
			break;
		}
	}

	public void DoPoisute (){
		_animateSprite = _spriteLoader.GetSpriteArray (SpriteLoader.CharacterType.POISUTE_DOING);
		_characterAnimator.PlayAnimation (_animateSprite, 2, WalkPoisute);
	}

	public void WalkPoisute (){
		_animateSprite = _spriteLoader.GetSpriteArray (SpriteLoader.CharacterType.POISUTE_WALKING);
		_characterAnimator.PlayAnimation (_animateSprite, 0);
	}
}
