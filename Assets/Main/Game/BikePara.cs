﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BikePara : MonoBehaviour {


	public Sprite bikePara;
	private Image hitoImage;
	public float speed = 1.8f;

	// Use this for initialization

	void Awake()
	{
		hitoImage = this.GetComponent<Image> ();
		hitoImage.sprite = bikePara;
	}



	void Update()
	{
		this.transform.Translate (0, -1*speed, 0);
	}
}
