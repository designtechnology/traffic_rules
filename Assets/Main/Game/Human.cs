﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Human : CharacterBehaviour {

	protected override void Awake()
	{
		base.Awake ();
		EvilizeHuman ();
	}

	void Start(){
		SetSpriteArray (SpriteLoader.CharacterType.HUMAN);
	}

	protected void EvilizeHuman(){
		int rand = Random.Range (0, 2);
		if (rand == 0) {
			if (this.transform.parent.GetComponent<EvilCheck> () == null && this.gameObject.GetComponent<EvilHuman>() == null) {
				this.transform.parent.gameObject.AddComponent<EvilCheck> ();
				EvilHuman _evil = this.gameObject.AddComponent<EvilHuman> () as EvilHuman;
				_evil.chara = this;
				isEvil = true;
			}
		}
	}
}
