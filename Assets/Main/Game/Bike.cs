﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Bike : CharacterBehaviour {


	protected override void Awake()
	{
		base.Awake ();
		EvilizeBike ();
	}

	void Start(){
		SetSprite (SpriteLoader.CharacterType.BIKE);
	}

	protected void EvilizeBike (){
		int rand = Random.Range (0, 2);
		if (rand == 0) {
			if (this.gameObject.GetComponent<EvilHuman>() == null) {
				EvilBike _evil = this.gameObject.AddComponent<EvilBike> () as EvilBike;
				_evil.chara = this;
				isEvil = true;
			}
		}
	}
}
