﻿using UnityEngine;
using System.Collections;

public class EvilBike : EvilBehaviour {

	public enum Evil
	{
		RANDOM,
		HEADPHONE,
		MUSHI,
		NIKETSU,
		YOKO
	}

	public Evil _state = Evil.RANDOM;

	protected override void Awake () {
		base.Awake ();
		if(_state == Evil.RANDOM)
			_state = (Evil)Random.Range (1, System.Enum.GetNames(typeof(Evil)).Length);
	}

	void Start(){
		AddEvilAbillity ();
	}

	protected override void AddEvilAbillity (){
		switch (_state) {
		case Evil.HEADPHONE:
			chara.point = 30;
			_image.sprite = _spriteLoader.GetSprite (SpriteLoader.CharacterType.HEADPHONE);
			break;
		case Evil.MUSHI:
			chara.ignoreFlg = true;
			chara.isEvil = false;
			break;
		case Evil.NIKETSU:
			chara.point = 50;
			_image.sprite = _spriteLoader.GetSprite (SpriteLoader.CharacterType.NIKETSU);
			break;
		case Evil.YOKO:
			chara.point = 50;
			_image.sprite = _spriteLoader.GetSprite (SpriteLoader.CharacterType.YOKO);
			_image.GetComponent<RectTransform> ().sizeDelta = new Vector2 (250, 300);
			break;
		default:
			break;
		}
	}
}
