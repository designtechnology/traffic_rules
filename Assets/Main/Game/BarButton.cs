﻿using UnityEngine;
using System.Collections;

public class BarButton : ToggleButton {

	public GameManager _gameManager;

	protected override void Awake (){
		base.Awake ();
		SetDelegates (_gameManager.PauseGame, _gameManager.ResumeGame);
	}
}
