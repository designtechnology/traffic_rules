﻿using UnityEngine;
using System.Collections;

public class Emitter : MonoBehaviour {

	public GameObject[] waves;
	public GameObject smoke;
	int currentWave;

	public void Emit(Transform point)
	{
		currentWave = Random.Range(0, waves.Length);	
		//instantiate wave
		GameObject g = Instantiate (waves [currentWave], point.position, Quaternion.identity) as GameObject;
		g.transform.parent = point.transform;
	}

	public void EmitSmoke(Transform point, Vector3 fixedPos)
	{
		GameObject g = Instantiate (smoke, point.position - fixedPos, Quaternion.identity) as GameObject;
		g.transform.parent = point.transform.parent.parent.parent;
	}
}