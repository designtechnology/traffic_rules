﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// スコア、コンボ、距離を管理するクラス
/// </summary>
public class GameManager : MonoBehaviour
{
	private SoundPlayer _soundPlayer;
	public StageController _stageController;
	public GameObject _pauseImage;
	private int _score;
	private int _combo;
	private float _distance;
	private float _startDistance = 500f; //開始時の残り距離
	public float startDistance{
		get { return _startDistance; }
		set { _startDistance = value; }
	}
	private float _speed = 0.2f;
	private bool _startFlg = false; //ゲーム開始のフラグ
	private bool _finishFlg = false; //ゲーム終了のフラグ
	private bool _pauseFlg = false; //ゲーム一時停止のフラグ
	[SerializeField]
	private Text _scoreText;
	[SerializeField]
	private Text _comboText;
	[SerializeField]
	private Text _distanceText;

//	アクセサ
	public float speed {
		get { return _speed; }
		set { _speed = value; }
	}

	void Awake ()
	{
		_soundPlayer = GetComponent<SoundPlayer> ();
	}

	void Start ()
	{
		InitGame ();
	}

	void Update ()
	{
		if (_startFlg && !_finishFlg && !_pauseFlg) {
			MoveStage ();
		} else if (_finishFlg) {
			//ゲーム終了時の処理
		}
	}

	/*
	 * ゲームの機能
	 */

	/// <summary> ゲームを初期化する </summary>
	private void InitGame ()
	{
		_startFlg = false;
		_finishFlg = false;
		ResetScore ();
		ResetCombo ();
		ResetDistance ();
	}

	public void StartGame ()
	{
		_startFlg = true;
		_soundPlayer.PlayBGM (SoundPlayer.BGMType.GAME);
	}

	public void FinishGame ()
	{
		_finishFlg = true;
		_soundPlayer.StopBGM ();
	}

	public void PauseGame ()
	{
		_pauseFlg = true;
		_pauseImage.SetActive (true);
		_stageController.PauseStage ();
	}

	public void ResumeGame ()
	{
		_pauseFlg = false;
		_pauseImage.SetActive (false);
		_stageController.ResumeStage ();
	}

	/*	
	 * スコア
	*/

	/// <summary> スコアを加算する </summary>
	public void PlusScore (int num)
	{
		if (_combo > 0)
			num =  (int) (num * (1f + 0.1f * _combo));
		_score += num;
		UpdateScore ();
	}

	/// <summary> スコアを減算する </summary>
	public void MinusScore (int num)
	{
		_score -= num;
		UpdateScore ();
	}

	public void ResetScore ()
	{
		_score = 0;
		UpdateScore ();
	}

	/// <summary> スコアの表示を更新する </summary>
	private void UpdateScore ()
	{
		_scoreText.text = "Score : " + _score.ToString ();
	}

	/*
	 * コンボ
	*/

	/// <summary> スコアを加算する </summary>
	public void PlusCombo ()
	{
		_combo++;
		UpdateCombo ();
	}

	public void ResetCombo ()
	{
		_combo = 0;
		UpdateCombo ();
	}

	/// <summary> スコアの表示を更新する </summary>
	private void UpdateCombo ()
	{
		if (_combo == 0)
			_comboText.text = "";
		else
			_comboText.text = _combo.ToString () + " Combo!";
	}

	/*
	 * 残り距離
	 */

	/// <summary> 距離を減算する </summary>
//	public void DecreaseDistance (float num)
//	{
//		_distance -= num;
//		UpdateDistance ();
//	}

	/// <summary> スピードに応じて距離を計算する </summary>
	private void MoveStage ()
	{
		_distance -= _speed;
		UpdateDistance ();
	}

	private void ResetDistance ()
	{
		_distance = _startDistance;
		UpdateDistance ();
	}

	private void UpdateDistance ()
	{
		_distanceText.text = "残り : " + ((int)_distance).ToString () + "m";
	}
}
