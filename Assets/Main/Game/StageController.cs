﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ステージの動作をコントロールするクラス
/// </summary>
public class StageController : MonoBehaviour {

	private GameManager _gameManager;
	private Emitter _emitter;
	//道パーツを配置するベースとなるRectTransform
	private RectTransform _roadBaseRect;
	//キャラクターを配置するベースとなるRectTransform
	private RectTransform _charaBaseRect;
	private RectTransform _stageRect;

	public GameObject _roadBlock;
	//道路の画像
	public Sprite road01;
	public Sprite road02;
	//横断歩道の画像
	public Sprite crosswalk;
	public Sprite station;

	private BoxCollider2D _trigger;

	public float interval = 200f;

	public float adjustedRate;

	private Vector2 imageSize {
		get {
			return new Vector2(1280f, 400f);
		}
	}

	private bool _stopFlg;

	//0,1⇒道路、2⇒横断歩道(信号赤)、9⇒駅
	private string _stageData = "01201012012012019";
//	private string _stageData = "000000000010000020009";
	private List<char> _stageDataList;

	//ステージのGameObjectを管理するリスト
	private List<GameObject> _stageList;

	void Awake (){
		//StageDataをListに入れる
		_stageDataList = new List<char> ();
		_stageList = new List<GameObject> ();
		foreach (char c in _stageData)
			_stageDataList.Add (c);

		_gameManager = this.transform.parent.parent.FindChild ("GameManager").GetComponent<GameManager> ();
		_emitter = _gameManager.GetComponent<Emitter> ();
		_roadBaseRect = this.transform.FindChild ("StageBase").GetComponent<RectTransform> ();
		_charaBaseRect = this.transform.FindChild ("CharacterBase").GetComponent<RectTransform> ();
		_stageRect = this.transform.GetComponent<RectTransform> ();

		CameraAdjuster _cameraAdjuster = Camera.main.GetComponent<CameraAdjuster> ();
		adjustedRate = _cameraAdjuster.adjustedRectRate;

		_stopFlg = false;
	}

	void Start () {
		InitStage ();
		MakeFirstStage ();
		EmitCharacters ();
		_gameManager.StartGame ();
//		for (int i = 0; i < _stageData.Length; i++)
//			AddStage ();
	}
	
	void Update () {
		if (!_stopFlg) {
			MoveStage ();
			if (_stageRect.anchoredPosition.y % interval == 0 && _stageDataList.Count != 0)
				EmitCharacters ();
		}
	}

	/// <summary> ステージを初期化する </summary>
	private void InitStage() {
		Vector2 _stageSize = new Vector2 (imageSize.x, imageSize.y * (_stageDataList.Count + 3));
		//ステージの大きさを初期化する
		_roadBaseRect.sizeDelta = _stageSize;
		_charaBaseRect.sizeDelta = _stageSize;
		_stageRect.sizeDelta = _stageSize;
	}

	/// <summary> ステージの最初の三枚を生成する </summary>
	private void MakeFirstStage (){
		for (int i = 0; i < 4; i++) {
//			Vector3 _blockPosition = new Vector3 (this.transform.position.x, this.transform.position.y + imageSize.y * i, this.transform.position.z);
			GameObject _block = Instantiate (_roadBlock, Vector3.zero, Quaternion.identity) as GameObject;
			RectTransform _blockRect = _block.GetComponent<RectTransform> ();
			_blockRect.anchoredPosition = new Vector2 (this.transform.position.x, this.transform.position.y + imageSize.y * i);
			_block.transform.parent = _roadBaseRect.transform;
			_block.AddComponent<RoadBlockController> ();
			_block.transform.SetAsLastSibling ();
			_stageList.Add (_block);
		}
	}

	private void EmitCharacters (){
		GameObject _emitArea = new GameObject ();
		_emitArea.transform.position = new Vector3 (this.transform.position.x, 800f * adjustedRate + 600f);
		_emitArea.transform.parent = _charaBaseRect.transform;
		_emitArea.AddComponent<EmitAreaController> ();
		_emitArea.transform.SetAsFirstSibling ();
		_emitter.Emit (_emitArea.transform);
	}

	/// <summary> ステージを動かす </summary>
	private void MoveStage() {
		this.transform.Translate (0f, -1 * _gameManager.speed * 10, 0f);
	}

	public void StopStage() {
		_stopFlg = true;
		_gameManager.FinishGame ();
	}

	public void PauseStage() {
		_stopFlg = true;
		_charaBaseRect.gameObject.SetActive (false);
	}

	public void ResumeStage() {
		_stopFlg = false;
		_charaBaseRect.gameObject.SetActive (true);
	}

	/// <summary> ステージを追加する </summary>
	public void AddStage() {
		if (_stageDataList.Count != 0) {
			int pos = _stageData.Length - _stageDataList.Count + 4;
			Vector3 _blockPosition = new Vector3 (this.transform.position.x, this.transform.position.y + imageSize.y * pos, this.transform.position.z);
			GameObject _block = Instantiate (_roadBlock, _blockPosition, Quaternion.identity) as GameObject;
			_block.transform.parent = _roadBaseRect.transform;
			RoadBlockController _blockController = _block.AddComponent<RoadBlockController> ();
			Image img = _block.GetComponent<Image> ();
			img.sprite = ChooseSprite (_stageDataList [0]);
			if (_stageDataList [0] == '2')
				MakeTrigger (_block);
			Debug.Log (_stageDataList [0]);
			_stageDataList.RemoveAt (0);
			_blockController.index = _stageDataList.Count;
			Debug.Log (_blockController.index);
			_stageList.Add (_block);
		}
	}

	private Sprite ChooseSprite (char c){
		switch (c) {
		case '0':
			return road01;
		case '1':
			return road02;
		case '2':
			return crosswalk;
		case '9':
			return station;
		default:
			return road01;
		}
	}

	public void RemoveStage (GameObject g){
		Destroy (g);
		_stageList.Remove (g);
	}

	private void MakeTrigger(GameObject g){
		_trigger = g.AddComponent<BoxCollider2D> ();
		_trigger.size = new Vector2 (1280, 100);
		_trigger.center = new Vector2 (0, 350);
		_trigger.isTrigger = true;
		g.tag = "Cross";
		g.AddComponent<Rigidbody2D> ().isKinematic = true;
	}
}
