﻿using UnityEngine;
using System.Collections;

public class EmitAreaController : MonoBehaviour {

	private StageController _stageController;

	void Awake(){
		_stageController = this.transform.parent.parent.GetComponent<StageController> ();
	}

	void Update () {
		if (this.transform.position.y + 900f < -400f * _stageController.adjustedRate) {
			Destroy (gameObject);
		}
	}
}
