﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class ToggleButton : MonoBehaviour {
	public delegate void firstDelegate();
	public firstDelegate firstMethod;
	public delegate void secondDelegate();
	public secondDelegate secondMethod;
	public Sprite firstImage;
	public Sprite secondImage;
	private Button _button;
	private bool toggle = false;

	protected virtual void Awake (){
		_button = GetComponent<Button> ();
		_button.onClick.AddListener(ToggleAction);
	}

	public void SetDelegates (firstDelegate first, secondDelegate second)
	{
		firstMethod = first;
		secondMethod = second;
	}

	private void ToggleAction ()
	{
		if (!toggle){
			firstMethod ();
			_button.image.sprite = secondImage;
		} else {
			secondMethod();
			_button.image.sprite = firstImage;
		}
		toggle = toggle ? false : true;
	}
}