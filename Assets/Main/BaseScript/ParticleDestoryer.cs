﻿using UnityEngine;
using System.Collections;

public class ParticleDestoryer : MonoBehaviour {

	//何秒後にDestroyするか
	public float finishTime = 2f;
	private float _startedTime;
	private float _currentTime;

	void Start () {
		_startedTime = Time.realtimeSinceStartup;
	}
	
	void Update () {
		_currentTime = Time.realtimeSinceStartup - _startedTime;
		if (_currentTime > finishTime)
			Destroy (this.gameObject);
	}
}
