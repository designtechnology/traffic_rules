﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIAnimator : MonoBehaviour
{
	private Image _objectImage;
	private Sprite[] _animateSpriteArray;
	private bool _playFlg = false;
	private int _updateRate = 12;
	private int _counter = 0;
	private int _currentFrame = 0;
	private int _loop = 0;
	private int _loopCount = 0;
	public delegate void onCompleteDelegate();
	public onCompleteDelegate onComplete;


	void Awake ()
	{
		_objectImage = this.transform.GetComponent<Image> ();
	}

	void Update ()
	{
		if (_playFlg) {
			_counter++;
			if (_counter == _updateRate) {
				UpdateAnimation ();
				_counter = 0;
			}
		}
	}

	/// <summary> フレームを更新する </summary>
	private void UpdateAnimation ()
	{
		_objectImage.sprite = _animateSpriteArray [_currentFrame];
		_currentFrame++;
		if (_currentFrame == _animateSpriteArray.Length) {
			_currentFrame = 0;
			if (_loop != 0) {
				_loopCount++; //1ループ終わったらカウントを増やす(0なら増やさない)
				if (_loopCount == _loop) {
					_playFlg = false; //全ループ終了で再生を停止
					if (onComplete != null)
						onComplete ();
				}
			}
		}
	}

	/// <summary> アニメーションを開始する </summary>
	/// <param name="spriteArray">アニメーションに使うSpriteの配列</param>
	/// <param name="loop">ループの回数（0で無限）</param>
	public void PlayAnimation (Sprite[] spriteArray, int loop)
	{
		_animateSpriteArray = spriteArray;
		_playFlg = true;
		_loopCount = 0;
		_loop = loop;
	}


	/// <summary> アニメーションを開始する </summary>
	/// <param name="spriteArray">アニメーションに使うSpriteの配列</param>
	/// <param name="loop">ループの回数（0で無限）</param>
	/// <param name="method">アニメーション終了時に呼び出すメソッド</param>
	public void PlayAnimation (Sprite[] spriteArray, int loop, onCompleteDelegate method)
	{
		this.PlayAnimation (spriteArray, loop);
		onComplete = method;
	}

	public void StopAnimation (){
		_playFlg = false;
	}
}
