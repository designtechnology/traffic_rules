﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

	public enum SEType
	{
		CORRECT,
		INCORRECT
	}

	public enum BGMType
	{
		GAME,
		TEST
	}

	public AudioClip correct;
	public AudioClip incorrect;
	public AudioClip gameBgm;
	public AudioClip gameBgm2;
	private AudioSource _seSource;
	private AudioSource _bgmSource;

	void Awake (){
		_seSource  = this.gameObject.AddComponent<AudioSource> ();
		_bgmSource = this.gameObject.AddComponent<AudioSource> ();
		_bgmSource.loop = true;
	}

	//BGMテスト用コード
	void Update (){
		if (Input.GetKeyDown ("0")) {
			StopBGM ();
			PlayBGM (BGMType.GAME);
		} else if (Input.GetKeyDown ("1")) {
			StopBGM ();
			PlayBGM (BGMType.TEST);
		}
	}

	public void PlaySound (AudioClip clip){
		_seSource.PlayOneShot (clip);
	}

	public void PlaySound (SEType type){
		switch (type){
		case SEType.CORRECT:
			_seSource.PlayOneShot (correct);
			break;
		case SEType.INCORRECT:
			_seSource.PlayOneShot (incorrect);
			break;
		default:
			break;
		}
	}

	public void PlayBGM (BGMType type){
		switch (type) {
		case BGMType.GAME:
			_bgmSource.clip = gameBgm;
			break;
		case BGMType.TEST:
			_bgmSource.clip = gameBgm2;
			break;
		default:
			break;
		}
		_bgmSource.Play ();
	}

	public void StopBGM() {
		_bgmSource.clip = null;
		_bgmSource.Stop ();
	}
}
