﻿using UnityEngine;
using System.Collections;

public class ScreenBehaviour : MonoBehaviour {

	private RectTransform _top;
	private RectTransform _bottom;

	void Start (){
		AdjustContents ();
	}

	protected void AdjustContents (){
		CameraAdjuster _cameraAdjuster = Camera.main.GetComponent<CameraAdjuster> ();
		float _adjustedRate = _cameraAdjuster.adjustedRectRate;

		Debug.Log (_adjustedRate);

		if (transform.FindChild ("Top") != null) {
			_top = transform.FindChild ("Top").GetComponent<RectTransform> ();
			_top.position = new Vector3 (_top.position.x, _top.position.y * _adjustedRate, 0f);
		}
		if (transform.FindChild ("Bottom") != null) {
			_bottom = transform.FindChild ("Bottom").GetComponent<RectTransform> ();
			_bottom.position = new Vector3 (_bottom.position.x, _bottom.position.y * _adjustedRate, 0f);
		}
	}
}
