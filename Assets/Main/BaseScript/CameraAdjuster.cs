﻿using UnityEngine;
using System.Collections;

/// <summary>
/// カメラのサイズを画面比によって切り替える
/// </summary>
public class CameraAdjuster : MonoBehaviour
{
	public RectTransform canvas;

	//標準の画面の縦横比
	private float _defaultRectRate;
	public float _adjustedRectRate;
	public float adjustedRectRate {
		get {
			return _adjustedRectRate;
		}
	}
	private Camera _camera;
	//private float _defaultCameraSize;

	void Awake ()
	{
		_defaultRectRate = canvas.rect.width / canvas.rect.height;
		_camera = this.gameObject.GetComponent<Camera> ();
		// _defaultCameraSize = _camera.orthographicSize;
		AdjustCameraSize ();
	}

	private void AdjustCameraSize ()
	{
		float _screenRate = (float)Screen.width / (float)Screen.height;
		_adjustedRectRate = _defaultRectRate / _screenRate;
		_camera.orthographicSize *= _adjustedRectRate;
	}
}
