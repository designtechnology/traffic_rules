﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(Image))]
public class CharacterBehaviour : MonoBehaviour {

	// 得点
	public int point = -10;

	// キャラクターの移動スピード
	public float speed = 1f;
	public bool isEvil = false;
	public bool isMoving = true;
	public bool ignoreFlg = false; //信号無視のフラグ

	// コンポーネント
	protected Image _image;
	protected Button _button;
	protected UIAnimator _characterAnimator;
	protected TapEffectController _tapEffectController;
	protected BoxCollider2D _boxCollider2D;

	// スプライトローダー
	private SpriteLoader _spriteLoader;
	// スプライトローダーから受け取るスプライト
	private Sprite[] _animateSprite;

	protected virtual void Awake()
	{
		_image = this.gameObject.GetComponent<Image> ();
		_button = this.gameObject.GetComponent<Button> ();
		_characterAnimator = this.gameObject.AddComponent<UIAnimator> ();
		_tapEffectController = this.gameObject.AddComponent<TapEffectController> ();
		_tapEffectController.chara = this;
		_boxCollider2D = this.gameObject.AddComponent<BoxCollider2D>();
		_spriteLoader = GameObject.Find ("GameManager").GetComponent<SpriteLoader> ();

		// Boxのサイズ
		_boxCollider2D.size = new Vector2 (100f, 300f);


		// ボタンリスナーにメソッドを登録
		if (_button != null)
			_button.onClick.AddListener (_tapEffectController.ObjectTapped);
	}

	void Update()
	{
		if(isMoving)
			this.transform.Translate (0, -1 * speed, 0);
	}

	/// <summary> スプライトを設定する </summary>
	protected void SetSprite(SpriteLoader.CharacterType type)
	{
		_image.sprite = _spriteLoader.GetSprite (type);
	}

	/// <summary> 複数のスプライトを設定し、アニメーションする </summary>
	protected void SetSpriteArray(SpriteLoader.CharacterType type)
	{
		_animateSprite = _spriteLoader.GetSpriteArray (type);
		_image.sprite = _animateSprite[0];
		if(!isEvil)
			_characterAnimator.PlayAnimation (_animateSprite, 0);
	}

	protected void OnBecameInvisible(){
		Destroy (this.gameObject);
	}

	protected void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "Cross") {
			if (!ignoreFlg){
				_characterAnimator.StopAnimation ();
				isMoving = false;
			}
			else {
				isEvil = true;
				point = 100;
			}
		}
	}
}
