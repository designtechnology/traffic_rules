﻿using UnityEngine;
using System.Collections;

/// <summary>
/// シーン間遷移などの管理を行うクラス
/// </summary>
public class SceneManager : MonoBehaviour {

	//シーンのGameObject
	public GameObject defaultScene;
	public GameObject titleScene;
	public GameObject gameScene;

	//シーン管理用enum
	public enum Scene
	{
		DEFAULT,
		TITLE,
		GAME
	}

	private GameObject _currentScene;

	void Start(){
		InitScene ();
	}

	private void InitScene(){
		_currentScene = defaultScene;
		_currentScene.SetActive (true);
	}

	/// <summary>
	/// 別のシーンに遷移する
	/// </summary>
	/// <param name="scene">対象シーン</param>
	public void ChangeScene (string scene){
		switch (scene) {
		case "DEFAULT":
			PushScene (defaultScene);
			break;
		case "TITLE":
			PushScene (titleScene);
			break;
		case "GAME":
			PushScene (gameScene);
			break;
		default:
			break;
		}
	}

	/// <summary>
	/// シーンを入れ替える
	/// </summary>
	public void PushScene (GameObject scene){
		_currentScene.SetActive (false);
		_currentScene = scene;
		_currentScene.SetActive (true);
	}
}