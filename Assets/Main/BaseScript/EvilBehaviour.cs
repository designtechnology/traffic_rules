﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

abstract public class EvilBehaviour : MonoBehaviour {

	public CharacterBehaviour chara;
	protected Sprite[] _animateSprite;
	protected SpriteLoader _spriteLoader;
	protected Image _image;
	protected UIAnimator _characterAnimator;
	protected TapEffectController _tapEffectController;

	protected virtual void Awake (){
		_spriteLoader = GameObject.Find ("GameManager").GetComponent<SpriteLoader> ();
		_image = this.gameObject.GetComponent<Image> ();
		_characterAnimator = this.gameObject.GetComponent<UIAnimator> ();
		_tapEffectController = this.gameObject.GetComponent<TapEffectController> ();
	}

	abstract protected void AddEvilAbillity ();
}
