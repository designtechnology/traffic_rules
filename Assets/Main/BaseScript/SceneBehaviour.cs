﻿using UnityEngine;
using System.Collections;

/// <summary>
/// シーンの基底クラス
/// </summary>
public class SceneBehaviour : MonoBehaviour {

	public GameObject defaultScreen;

//	public GameObject testScreen;

	protected SceneManager _sceneManager;

	protected GameObject _currentScreen;

	protected void Awake () {
		//SceneManagerをFindする
		if(_sceneManager == null)
			_sceneManager = transform.parent.FindChild ("SceneManager").GetComponent<SceneManager> ();
	}

	protected void OnEnable () {
		//デフォルトのスクリーンに遷移する
		PushScreen (defaultScreen);
	}

	/// <summary>
	/// スクリーンを入れ替える
	/// </summary>
	protected void PushScreen (GameObject screen){
		if(_currentScreen != null)
			Destroy (_currentScreen);
		_currentScreen = Instantiate (screen) as GameObject;
		_currentScreen.transform.parent = this.transform;
	}

	/*画面遷移テスト用コード
	void Update(){
		if (Input.GetKeyDown ("0")) {
			ChangeScreen ("Default");
		}
		else if (Input.GetKeyDown ("1")) {
			ChangeScreen ("Game");
		}
	}
	
	protected void ChangeScreen (string name){
		if (name == "Default")
			PushScreen (defaultScreen);
		else if (name == "Test")
			PushScreen (testScreen);
	}
	*/
}
