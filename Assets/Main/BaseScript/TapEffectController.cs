﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;

public class TapEffectController : MonoBehaviour {

	public CharacterBehaviour chara;
	private GameManager _gameManager;
	private Emitter _emitter;
	private SoundPlayer _soundPlayer;
	private SpriteLoader _spriteLoader;
	private RectTransform _charaRect;
	private Button _button;
	private bool _isEvil{
		get {
			return chara.isEvil;
		}
	}
	private int _point;
	public int point {
		get { return _point; }
		set { _point = value; }
	}

	void Awake (){
		_gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		_emitter = _gameManager.gameObject.GetComponent<Emitter> ();
		_soundPlayer = _gameManager.gameObject.GetComponent<SoundPlayer> ();
		_spriteLoader = _gameManager.gameObject.GetComponent<SpriteLoader> ();
		_charaRect = this.gameObject.GetComponent<RectTransform> ();
		_button = this.gameObject.GetComponent<Button> ();
	}

	public void ObjectTapped ()
	{
		_button.interactable = false; //タップしても反応しないようにする
		_gameManager.PlusScore (chara.point);

		if (_isEvil) {
			EvilEffect ();
			ShowAnswer (_spriteLoader.correctSprite);
			_soundPlayer.PlaySound (SoundPlayer.SEType.CORRECT);
			_gameManager.PlusCombo ();
		} else {
			JusticeEffect ();
			ShowAnswer (_spriteLoader.incorrectSprite);
			_soundPlayer.PlaySound (SoundPlayer.SEType.INCORRECT);
			_gameManager.ResetCombo ();
		}
	}

	/// <summary>
	/// いい人だった時のエフェクト
	/// </summary>
	private void JusticeEffect ()
	{
	}

	/// <summary>
	/// 悪い人だった時のエフェクト
	/// </summary>
	private void EvilEffect ()
	{
		HOTween.To(_charaRect, 0.5f, new TweenParms().Prop("anchoredPosition", _charaRect.anchoredPosition + new Vector2(0f, 600f)).Prop("sizeDelta", _charaRect.sizeDelta*5f).Ease(EaseType.EaseInExpo));
		HOTween.To (_charaRect.GetComponent<Image> (), 0.3f, new TweenParms().Prop("color", new Color (1f, 1f, 1f, 0f)).Delay(0.2f).OnComplete(DestroyTweenedObject, _charaRect.gameObject));

		_emitter.EmitSmoke (transform, new Vector3 (0f,130f));
	}

	private void ShowAnswer(Sprite sprite){
		GameObject answer = new GameObject ();
		answer.name = "Answer";
		answer.transform.parent = this.transform;
		answer.transform.position = this.transform.position;

		//大きさ
		RectTransform rect = answer.AddComponent<RectTransform> ();
		rect.sizeDelta = new Vector2 (400, 400);

		//表示する画像
		Image img = answer.AddComponent<Image> ();
		img.sprite = sprite;
		img.color = new Color (1f, 1f, 1f, 0f);

		float tweenTime = 0.5f;

		HOTween.To (rect, tweenTime, new TweenParms().Prop("sizeDelta", rect.sizeDelta / 2f).Ease(EaseType.EaseOutExpo));
		HOTween.To (img, tweenTime, "color", Color.white);
		HOTween.To (img, tweenTime, new TweenParms().Prop( "color", new Color (1f,1f,1f,0f)).Delay(1f).OnComplete(DestroyTweenedObject,answer));
	}

	/// <summary> HOTweenのパラメータで指定したGameObjectを削除する </summary>
	/// <param name="data">Data.</param>
	public void DestroyTweenedObject(TweenEvent data){
		Destroy ((GameObject)data.parms [0]);
	}

	private void EvilTapped(){

	}
}
