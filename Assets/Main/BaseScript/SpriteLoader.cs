﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteLoader : MonoBehaviour {

	public enum CharacterType
	{
		HUMAN,
		SMARTPHONE,
		TOBACCO,
		POISUTE_WALKING,
		POISUTE_DOING,
		BIKE,
		HEADPHONE,
		NIKETSU,
		YOKO,
		CAR
	}
		
	private List<Sprite[]> _animateSpriteList;
	private List<Sprite[]> _tobaccoSpriteList;
	private List<Sprite[]> _smartphoneSpriteList;
	private List<Sprite[]> _poisuteWalkingSpriteList;
	private List<Sprite[]> _poisuteDoingSpriteList;
	private List<Sprite> _bikeSpriteList;
	private List<Sprite> _headphoneSpriteList;
	private List<Sprite> _niketsuSpriteList;
	private List<Sprite> _yokoSpriteList;
	private List<Sprite> _carSpriteList;

	//正解・不正解のスプライト
	[SerializeField]
	private Sprite _correctSprite;
	[SerializeField]
	private Sprite _incorrectSprite;
	public Sprite correctSprite{
		get { return _correctSprite; }
	}
	public Sprite incorrectSprite{
		get { return _incorrectSprite; }
	}

	void Awake () {
		_animateSpriteList = new List<Sprite[]> ();
		_tobaccoSpriteList = new List<Sprite[]> ();
		_smartphoneSpriteList = new List<Sprite[]> ();
		_poisuteWalkingSpriteList = new List<Sprite[]> ();
		_poisuteDoingSpriteList = new List<Sprite[]> ();
		_bikeSpriteList = new List<Sprite> ();
		_headphoneSpriteList = new List<Sprite> ();
		_niketsuSpriteList = new List<Sprite> ();
		_yokoSpriteList = new List<Sprite> ();
		_carSpriteList = new List<Sprite> ();

		GetSpriteArrayList (17, _animateSpriteList, "", "Image/Hito/");
		GetSpriteArrayList (3, _tobaccoSpriteList, "taba", "Image/Hito/");
		GetSpriteArrayList (3, _smartphoneSpriteList, "suma", "Image/Hito/");
		GetSpriteArrayList (1, _poisuteWalkingSpriteList, "poiwalk", "Image/Hito/");
		GetSpriteArrayList (1, _poisuteDoingSpriteList, "poido", "Image/Hito/");
		GetSpriteList (10, _bikeSpriteList, "bike", "Image/Bike/");
		GetSpriteList (3, _headphoneSpriteList, "head", "Image/Bike/");
		GetSpriteList (1, _niketsuSpriteList, "niketsu", "Image/Bike/");
		GetSpriteList (1, _yokoSpriteList, "yoko", "Image/Bike/");
		GetSpriteList (3, _carSpriteList, "car", "Image/Car/");
	}

	private void GetSpriteArrayList (int num, List<Sprite[]> spList, string name, string location){
		Sprite[] spArray = Resources.LoadAll<Sprite>(location);
		for (int i = 0; i < num; i++) {
			Sprite[] _pair = new Sprite[2];
			string spBack = ((char)((char)i + 'a')).ToString ();
			string spName = name + spBack;
			_pair [0] = System.Array.Find<Sprite> (spArray, (sprite) => sprite.name.Equals (spName + "1"));
			_pair [1] = System.Array.Find<Sprite> (spArray, (sprite) => sprite.name.Equals (spName + "2"));
//			Debug.Log (spName + "1");
			if(_pair != null)
				spList.Add (_pair);
		}
	}

	private void GetSpriteList (int num, List<Sprite> spList, string name, string location){
		Sprite[] spArray = Resources.LoadAll<Sprite>(location);
		for (int i = 0; i < num; i++) {
			Sprite _tempSprite;
			string spBack = "_" + ((char)((char)i + 'a')).ToString ();
			string spName = name + spBack;
			_tempSprite = System.Array.Find<Sprite> (spArray, (sprite) => sprite.name.Equals (spName));
			Debug.Log (location + spName + ".png");
			if(_tempSprite != null)
				spList.Add (_tempSprite);
		}
	}

	public Sprite[] GetSpriteArray (CharacterType type){
		int rand;
		switch (type) {
		case CharacterType.HUMAN:
			rand = Random.Range (0, _animateSpriteList.Count);
			return _animateSpriteList [rand];
		case CharacterType.SMARTPHONE:
			rand = Random.Range (0, _smartphoneSpriteList.Count);
			return _smartphoneSpriteList [rand];
		case CharacterType.TOBACCO:
			rand = Random.Range (0, _tobaccoSpriteList.Count);
			return _tobaccoSpriteList [rand];
		case CharacterType.POISUTE_WALKING:
			return _poisuteWalkingSpriteList [0];
		case CharacterType.POISUTE_DOING:
			return _poisuteDoingSpriteList [0];
		default:
			return new Sprite[0];
		}
	}

	public Sprite GetSprite (CharacterType type){
		int rand;
		switch (type) {
		case CharacterType.BIKE:
			rand = Random.Range (0, _bikeSpriteList.Count);
			return _bikeSpriteList [rand];
		case CharacterType.HEADPHONE:
			rand = Random.Range (0, _headphoneSpriteList.Count);
			return _headphoneSpriteList [rand];
		case CharacterType.NIKETSU:
			rand = Random.Range (0, _niketsuSpriteList.Count);
			return _niketsuSpriteList [rand];
		case CharacterType.YOKO:
			rand = Random.Range (0, _yokoSpriteList.Count);
			return _yokoSpriteList [rand];
		case CharacterType.CAR:
			rand = Random.Range (0, _carSpriteList.Count);
			return _carSpriteList [rand];
		default:
			return new Sprite();
		}
	}
}
